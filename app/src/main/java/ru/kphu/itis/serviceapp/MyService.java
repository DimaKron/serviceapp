package ru.kphu.itis.serviceapp;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by Дмитрий on 02.10.2017.
 */

public class MyService extends Service {

    private static final String LOG_TAG = "MyService";


    public static Intent makeIntent(@NonNull Context context){
        return new Intent(context, MyService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartCommand");
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0; i<10; i++){
                    try {
                        Thread.sleep(1000);
                        Log.d(LOG_TAG, i+ " seconds left");
                    } catch (InterruptedException e) {
                        Log.e(LOG_TAG, e.getMessage());
                    }
                }
                stopSelf();
            }
        }).start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d(LOG_TAG, "onDestroy");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
