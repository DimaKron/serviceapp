package ru.kphu.itis.serviceapp;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

/**
 * Created by Дмитрий on 02.10.2017.
 */

public class MyIntentService extends IntentService {

    private static final String LOG_TAG = "MyIntentService";

    private static final String ACTION_HELLO = "ru.kphu.itis.serviceapp.HELLO";

    private static final int NOTIFICATION_ID = 222;

    public static Intent makeIntent(@NonNull Context context){
        return new Intent(context, MyIntentService.class);
    }

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        //showNotification("hello");

        for (int i=0; i<10; i++){
            try {
                Thread.sleep(1000);
                Log.d(LOG_TAG, i+ " seconds left");
            } catch (InterruptedException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }

        sendBroadcast(new Intent(ACTION_HELLO));
    }

    private void showNotification(String string){
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Notification notification = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentText(string)
                .setContentTitle(string)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .build();

        notificationManager.notify(NOTIFICATION_ID, notification);
    }
}
