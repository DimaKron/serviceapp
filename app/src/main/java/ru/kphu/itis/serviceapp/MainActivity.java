package ru.kphu.itis.serviceapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final long DELAY = 5000;

    private static final int REQUEST_CODE_DELAYED_SERVICE = 111;

    private Button startButton;

    private Button stopButton;

    private Button startDelayed;

    private TextView batteryTextView;

    private ProgressBar progressBar;

    private Handler handler;

    private int counter;

    private BroadcastReceiver batteryReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        batteryTextView = (TextView) findViewById(R.id.text_view_battery_level);

        batteryReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                batteryTextView.setText(String.valueOf(intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 1.0 / intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1) * 100));
            }
        };
        registerReceiver(batteryReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        startButton = (Button) findViewById(R.id.button_start);
        startButton.setOnClickListener(this);

        startDelayed = (Button) findViewById(R.id.button_start_delayed);
        startDelayed.setOnClickListener(this);

        stopButton = (Button) findViewById(R.id.button_stop);
        stopButton.setOnClickListener(this);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setMax(10);

        int counter = 0;
        handler  = new Handler();
        startLoading();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(batteryReceiver);
        super.onDestroy();
    }

    private void startLoading() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(counter < 10) {
                    progressBar.setProgress(counter + 1);
                    counter++;
                    handler.postDelayed(this, 1000);
                }
            }
        }, 1000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_start:
                onStartClick();
                break;
            case R.id.button_start_delayed:
                onStartDelayedClick();
                break;
            case R.id.button_stop:
                onStopClick();
                break;
        }
    }

    private void onStartClick(){
        //startService(MyService.makeIntent(this));
        startService(MyIntentService.makeIntent(this));
    }

    private void onStartDelayedClick() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getService(this, REQUEST_CODE_DELAYED_SERVICE, MyIntentService.makeIntent(this), PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + DELAY, pendingIntent);
    }

    private void onStopClick() {
        //stopService(MyService.makeIntent(this));
        stopService(MyIntentService.makeIntent(this));
    }
}
