package ru.kphu.itis.serviceapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.support.v7.app.NotificationCompat;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Дмитрий on 09.10.2017.
 */

public class HelloBroadcastReceiver extends BroadcastReceiver {

    private static final int NOTIFICATION_ID = 333;

    @Override
    public void onReceive(Context context, Intent intent) {
        showNotification(context, "HELLO");
    }

    private void showNotification(Context context, String string){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        Notification notification = new NotificationCompat.Builder(context)
                .setAutoCancel(true)
                .setContentText(string)
                .setContentTitle(string)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .build();

        notificationManager.notify(NOTIFICATION_ID, notification);
    }
}
